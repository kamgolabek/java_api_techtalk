package com.kamilg.techtalk.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.kamilg.techtalk.model.User;

public interface UsersRepository extends JpaRepository<User, Integer > {

}
