package com.kamilg.techtalk.api;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.kamilg.techtalk.model.User;
import com.kamilg.techtalk.repository.UsersRepository;

@RestController
@RequestMapping("/api/users")
public class UsersApi {

	@Autowired
	UsersRepository users;

	@GetMapping
	public List<User> allUsers() throws Exception {
		return users.findAll();
	}

	@PostMapping
	public ResponseEntity<?> createUser(@RequestBody User user) {
		Optional<User> exisisting = users.findById(user.getId());
		if(exisisting.isPresent()) {
			return new ResponseEntity<>("User with ID " + exisisting.get().getId() + " already exists.", HttpStatus.CONFLICT); // or 400 bad request :   https://stackoverflow.com/questions/3825990/http-response-code-for-post-when-resource-already-exists
		}
		User newUser = users.save(user);

		return new ResponseEntity<>(newUser, HttpStatus.CREATED);
	}

}
